# Exchange Operator

## Minimal description

Suppose we have data on which we want to call a SYCL kernel, and we can split arbitrarily horizontally this data. Instead of transfering the complete data at once to the target device, we can instead laverage asynchronous memory copies in SYCL.

In particular, via using multiple queues (equivalent of CUDA streams), we would have (in an ideal scenario) instead of:

| queue | task + its duration | task + its duration |
| --- | --- | --- |
| #0 | h->d + 101 ms   | kernel + 101 ms  |

Here, the x-axis is the time, and hence it takes in total 202 ms to complete the operation.

For the purpose of example, suppose the transfer/kernel requires 1ms to be launched, and 100ms to complete.

We would split the data into say 4 partitions and have:

| queue | task + its duration | task + its duration | task + its duration | task + its duration | task + its duration |
| --- | --- | --- | --- | --- | --- |
| #0 | h->d (pt[0]) + 26 ms   | h->d (pt[1]) + 26 ms   | h->d (pt[2]) + 26 ms   | h->d (pt[3]) + 26 ms   |   |
| #1 |   | kernel (pt[0]) + 26 ms  | kernel (pt[1]) + 26 ms  | kernel (pt[2]) + 26 ms  | kernel (pt[3]) + 26 ms  |

Again, the x-axis is the time, and hence it takes in total `(4+1)*(25+1) = 130` ms to complete the operation. Easy, 1.55 speed up?!

What if there are 10 partititons - `(10+1) * (10+1) = 121` ms to complete the task, or 1.66 speed up?!

And 100, `(100+1) * (1+1) = 202`, then initialization costs become visible. But is it only that?

## Caveats

* In reality, small partition sizes might not saturate the PCI bandwidth, and hence the transfer might be slower in these cases. Solution - start transfering 2*i KB for increasing integer i, until a minimal size saturating the PCI bandwidth is reached. Empirically, on several CUDA GPUs that is around 100 MB.

* Moreover, in reality there might be a significant difference between the kernel and the transfer time. In the DB workflows we encounter the kernel time is usually much smaller than the transfer time. In this case, suppose `T` is the required time for the full transfer, and `K` is the required time for the full kernel. Then, the new running time would roughly be `T + (K/N)` where, `N` is the number of partitions (disregarding initilization costs, which are insignificant, and assuming that `N` provides sufficiently big partitions for optimal transfers). We end up "hiding" the faster time (in this case, the kernel time), but we again need the "full" time to transfer the slower part.


## Overcoming device memory limitations

Notice that with the exchange operator we are simulatenously working with only 2 partitions at a time. Roughly, we are transfering the `i`-th partition, while we are calling the kernel for the `(i+1)`-th partition. Hence, we do not need to allocate arrays with the full data size on the device, but only of size of 2 partitions. This allows to process data not fitting in device memory.

## Running on multiple devices

We can expand the concept of the exchange to arbitrary number of devices. We initially needed a counter to keep track of the current partition. Now, we make a global atomic counter, which is going to be read and updated accordingly from each target device.

To handle hash joins, we build the full hash table on every target device. Expectation is that the `build size << probe size`.

## Storing as well

Additionally, one could parallelize the store operation as well, so asynchroniously doing:
* h->d transfer of (i+1)-th partition
* kernel call on the i-th partition
* d->h transfer of the (i-1)-th partition

To satisfy the SSB benchmarks it suffices find sums of small arrays, and hence, our current exchange does not support yet parallel store as well.

## Implementation

* Each target device would have 2 queues, sharing the same context.
* Before launching a multithreaded call to the different devices, we initialize an atomic counter `std::atomic<int> interval_idx(0);`.
* Then we run on the devices in parallel as:
```cpp
  
  for (int gpu_id = 0; gpu_id < n_gpus; gpu_id++) {
    threads.push_back(std::thread([&, gpu_id]() {
      // 1. in case of hash join, create build tables on each device
      // ...
      //
      // 2. allocate 2x device partition size for the horizontally split data
      // (either probe in hash join case, or projection itself)
      // ...
      // 3. exchange, "roughly" as:
      while (interval_idx < n_partitions) {
        for (int i = 0; i < 2; i++) { // because of step 2
          int this_idx = interval_idx++;
          if (this_idx >= n_partitions)
            break;
          this_partition_size = (this_idx == n_partitions - 1)
                                        ? last_partition_size
                                        : partition_size;
          for (int j = 1; j < proj.n_columns; j++) {
            // 4. h->d transfer of this_idx-th partition on queue 0
            // (at the first or second hald of the devic array, depending on i)
          }
          // ensure this_idx-th partition is transfered before kernel call
          queue[gpu_id][0].wait();
          // ensure previous kernel call is finished before starting the next kernel call
          queue[gpu_id][1].wait();
          // 5. kernel call on this_idx-th partition on queue 1
          // (should not wait in the end of the loop, as we want to start the next transfer, during the current kernel call)
        }
        queue[gpu_id][1].wait(); // wait for the last kernel call to finish
        // d->h if required (typically 1 element)
      }
    }));
  }
  for (auto &t : threads) {
    t.join();
  }
```


## Expanded and a more "complete" example

See the sycldb folder. You can find join and projection benchmarks, which pass their kernels to an exchange operator, taking care of the asynchronous execution.

You can see the readme inside of sycldb and directly run the standalone join and projection laveraging the exchange operator.

## Adapting arbitrary benchmark to exchange

That would require encapsulating the data in a struct `ProbeData` as for instance in the `ssb/projection` benchmark:

```cpp
  ProbeData<float, float> prob; // <input, output> column(s) types
  prob.n_probes = 2; // number of input columns
  prob.h_lo_data = new float *[prob.n_probes];
  prob.h_lo_data[0] = NULL; // should be allocated on host, before the exchange
  prob.h_lo_data[1] = NULL; // should be allocated on host, before the exchange
  prob.len_each_probe = num_items; // size of each input column
  prob.res_size = num_items; // size of the output column, which is here a shared array
  // res_array_cols and res_idx are for the cases when the output represents multiple columns
  // for instance res_array_cols 4 and res_idx 2 would mean that the output is 4 columns, and we are summing the results of the 3rd column
  // here we have only one column, so we set res_array_cols to 1 and res_idx to 0
  // (obviously res_idx >= res_array_cols is not valid)
  prob.res_array_cols = 1;
  prob.res_idx = 0;
  // in the function, the third argument is the build tables, which are not used in the projection
  // see the join benchmark how to pass them
  // and the partition_len is NOT the input full size, it is going to be set inside of the exchange
  prob.probe_function = [&](float **probe_data, int partition_len, float **,
                            float *res, sycl::queue queue,
                            sycl::event &event) {
      sycl::range<1> gws((partition_len + TILE_ITEMS - 1) / TILE_ITEMS *
                         N_BLOCK_THREADS);
      sycl::range<1> lws(N_BLOCK_THREADS);
      event = queue.submit([&](sycl::handler &cgh) {
        float *probe_data_ct0 = probe_data[0];
        float *probe_data_ct1 = probe_data[1];
        // call kernel as usual, but with updated arguments
        // these arguments will be assigned inside of the exchange
        // important is to pass the function, here projectSigmoid
        cgh.parallel_for(
            sycl::nd_range<1>(gws, lws), [=](sycl::nd_item<1> item_ct1) {
              projectSigmoid<N_BLOCK_THREADS, N_ITEMS_PER_THREAD>(
                  probe_data_ct0, probe_data_ct1, res, partition_len, item_ct1);
            });
      });
  };
```

Note: running benchmarks with 2d ranges `sycl::nd_range<1>(gws, lws)`, is optimization unrelated to the exchange operator. If the kernel is normally run as `sycl::range<1>`, then they could/should run the same.